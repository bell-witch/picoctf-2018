# Crypto Warmup 1
## Solve
Crpyto can often be done by hand, here's a message you got from a friend, llkjmlmpadkkc with the key of thisisalilkey. Can you use this [table](./table.txt) to solve it?. 

## Hints
Submit your answer in our competition's flag format. For example, if you answer was 'hello', you would submit 'picoCTF{HELLO}' as the flag.  
Please use all caps for the message.

## Solution
simply following the table will get us our flag: `picoCTF{SECRETMESSAGE}`