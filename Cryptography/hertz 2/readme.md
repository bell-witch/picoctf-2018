# hertz 2
## Solve
This flag has been encrypted with some kind of cipher, can you decrypt it? Connect with nc 2018shell3.picoctf.com 12521. 

## Hints
These kinds of problems are solved with a frequency that merits some analysis.

## Solution
connecting we can get our ciphertext
```
Neg ctdvo ywbqs ibl ktjph bmgw neg rxzf uba. D vxs'n ygrdgmg nedh dh htve xs gxhf pwbyrgj ds Pdvb. Dn'h xrjbhn xh di D hbrmgu x pwbyrgj xrwgxuf! Boxf, idsg. Egwg'h neg irxa: pdvbVNI{htyhndntndbs_vdpegwh_xwg_nbb_gxhf_tqetiyssft}
```
since the problem doesnt tell us what cipher this is, we need to figure it out. to do so we find the [index of coincidence](https://en.wikipedia.org/wiki/Index_of_coincidence). i used [this](http://practicalcryptography.com/cryptanalysis/text-characterisation/index-coincidence/) tool to do so. an index of coincidence near 0.06 is likely a substituion cipher. our text resulted in an index of 0.052, meaning its more than likely a substituion cipher. plugging it into our [solver](https://www.guballa.de/substitution-solver) will get us our flag: `picoCTF{substitution_ciphers_are_too_easy_uwhu
fbnnyu}`