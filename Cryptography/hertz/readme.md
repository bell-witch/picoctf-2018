# hertz
## Solve
Here's another simple cipher for you where we made a bunch of substitutions. Can you decrypt it? Connect with nc 2018shell3.picoctf.com 48186. 

## Hints
NOTE: Flag is not in the usual flag format

## Solution
using [this](https://www.guballa.de/substitution-solver) substitution solver gives us the flag: `substitution_ciphers_are_solvable_uwhufbnnyu`