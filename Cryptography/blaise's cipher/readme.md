# blaise's cipher
## Solve
My buddy Blaise told me he learned about this cool cipher invented by a guy also named Blaise! Can you figure out what it says? Connect with nc 2018shell3.picoctf.com 46966

## Hints
There are tools that make this easy.  
This cipher was NOT invented by Pascal

## Solution
googling the name we quickly find out its a `Blaise de Vigenère` cipher. using [this](https://www.guballa.de/vigenere-solver) tool we can search the text and easily get our flag: `picoCTF{v1gn3r3_c1ph3rs_ar3n7_bad_cdf08bf0}`