# caesar cipher 1
## Solve
This is one of the older ciphers in the books, can you decrypt the [message](./ciphertext)? You can find the ciphertext in /problems/caesar-cipher-1_4_e4dc6dcfb004bdade0b9ce8e44f1bac4 on the shell server. 

## Hints
caesar cipher [tutorial](https://learncryptography.com/classical-encryption/caesar-cipher)

## Solution
there are tons of ways to solve caesar cipher's online, but i used [cyberchef](https://gchq.github.io/CyberChef/) with an amount of 6 to get the flag: `picoCTF{justagoodoldcaesarciphertobrvmri}`