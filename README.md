# Table of Contents
- [picoCTF 2018](#picoctf-2018)
- [Programs](#programs)
- [Problems](#problems)
    - [Forensics](#forensics)
    - [General Skills](#general-skills)
    - [Reversing](#reversing)
    - [Cryptography](#cryptography)
    - [Web Exploitation](#web-exploitation)
    - [Binary Exploitation](#binary-exploitation)

# picoCTF 2018
this is my write up for my first CTF ever :-D  
i managed to finish 51 problems out of 78 for a total of 8635 points

# Programs
these are the non-web based programs i used  
[firefox](https://www.mozilla.org/en-US/)  
[wireshark](https://www.wireshark.org/)  
[linux](https://distrowatch.com/dwres.php?resource=major)  
[vscode](https://code.visualstudio.com/) (for the asm/python stuff)

# Problems
## Forensics
| Name                                                                  | Points | Solves |
|:----------------------------------------------------------------------|:------:|:------:|
| [Forensics Warmup 1](/Forensics/Forensics%20Warmup%201)               |   50   | 10400  |
| [Forensics Warmup 2](/Forensics/Forensics%20Warmup%202)               |   50   |  9857  |
| [Desrouleaux](/Forensics/Desrouleaux)                                 |  150   |  2435  |
| [Reading Between the Eyes](/Forensics/Reading%20Between%20the%20Eyes) |  150   |  2188  |
| [admin panel](/Forensics/admin%20panel)                               |  150   |  4078  |
| [hex editor](/Forensics/hex%20editor)                                 |  150   |  3998  |
| [Truly an Artist](/Forensics/Truly%20an%20Artist)                     |  200   |  3427  |
| [now you don't](/Forensics/now%20you%20don%27t)                       |  200   |  3428  |
| [Lying Out](/Forensics/Lying%20Out)                                   |  250   |  1202  |
| [What's My Name?](/Forensics/What%27s%20My%20Name)                    |  250   |  2268  |
## General Skills
| Name                                                            | Points | Solves |
|:----------------------------------------------------------------|:------:|:------:|
| [General Warmup 1](/General%20Skills/General%20Warmup%201)      |   50   | 12643  |
| [General Warmup 2](/General%20Skills/General%20Warmup%202)      |   50   | 12522  |
| [General Warmup 3](/General%20Skills/General%20Warmup%203)      |   50   | 12343  |
| [Resources](/General%20Skills/Resources)                        |   50   | 11238  |
| [grep 1](/General%20Skills/grep%201)                            |   75   |  8829  |
| [net cat](/General%20Skills/net%20cat)                          |   75   |  7937  |
| [strings](/General%20Skills/strings)                            |  100   |  6040  |
| [pipe](/General%20Skills/pipe)                                  |  100   |  5485  |
| [grep 2](/General%20Skills/grep%202)                            |  125   |  5225  |
| [Aca-Shell-A](/General%20Skills/Aca-Shell-A)                    |  125   |  3559  |
| [environ](/General%20Skills/environ)                            |  150   |  3754  |
| [ssh-keyz](/General%20Skills/ssh-keyz)                          |  150   |  3218  |
| [what base is this?](/General%20Skills/what%20base%20is%20this) |  200   |  3443  |
| [you can't see me](/General%20Skills/you%20can%27t%20see%20me)  |  200   |  2448  |
| [absolutely relative](/General%20Skills/absoultely%20relative)  |  250   |  1763  |
| [in out error](/General%20Skills/in%20out%20error)              |  275   |  1663  |
| [store](/General%20Skills/store)                                |  400   |  1824  |
## Reversing
| Name                                                    | Points | Solves |
|:--------------------------------------------------------|:------:|:------:|
| [Reversing Warmup 1](/Reversing/Reversing%20Warmup%201) |   50   |  7314  |
| [Reversing Warmup 2](/Reversing/Reversing%20Warmup%202) |   50   |  8730  |
| [assembly-0](/Reversing/assembly-0)                     |  150   |  2139  |
| [assembly-1](/Reversing/assembly-1)                     |  200   |  1537  |
| [assembly-2](/Reversing/assembly-2)                     |  250   |  952   |
## Cryptography
| Name                                                 | Points | Solves |
|:-----------------------------------------------------|:------:|:------:|
| [Crypto Warmup 1](/Cryptography/Crypto%20Warmup%201) |   75   |  5586  |
| [Crypto Warmup 2](/Cryptography/Crypto%20Warmup%202) |   75   |  8396  |
| [caesar cipher 1](/Cryptography/caesar%20cipher%201) |  150   |  4797  |
| [hertz](/Cryptography/hertz)                         |  150   |  2894  |
| [blaise's cipher](/Cryptography/blaise%27s%20cipher) |  200   |  2826  |
| [hertz 2](/Cryptography/hertz%202)                   |  200   |  2254  |
| [caesar cipher 2](/Cryptography/caesar%20cipher%202) |  250   |  952   |
## Web Exploitation
| Name                                                                                   | Points | Solves |
|:---------------------------------------------------------------------------------------|:------:|:------:|
| [Inspect Me](/Web%20Exploitation/Inspect%20Me)                                         |  125   |  7395  |
| [Client Side is Still Bad](/Web%20Exploitation/Client%20Side%20is%20Still%20Bad)       |  150   |  6573  |
| [Logon](/Web%20Exploitation/Logon)                                                     |  150   |  3607  |
| [Irish Name Repo](/Web%20Exploitation/Irish%20Name%20Repo)                             |  200   |  3351  |
| [Mr. Robots](/Web%20Exploitation/Mr.%20Robots)                                         |  200   |  3439  |
| [Secret Agent](/Web%20Exploitation/Secret%20Agent)                                     |  200   |  2326  |
| [Buttons](/Web%20Exploitation/Buttons)                                                 |  250   |  2767  |
| [The Vault](/Web%20Exploitation/The%20Vault)                                           |  250   |  2120  |
| [Artisinal Handcrafted HTTP 3](/Web%20Exploitation/Artisinal%20Handcrafted%20HTTP%203) |  300   |  782   |
| [fancy-alive-monitoring](/Web%20Exploitation/fancy-alive-monitoring)                   |  400   |  565   |
## Binary Exploitation
| Name                                                              | Points | Solves |
|:------------------------------------------------------------------|:------:|:------:|
| [buffer overflow 0](/Binary%20Exploitation/buffer%20overflow%200) |  150   |  2917  |
| [leak-me](/Binary%20Exploitation/leak-me)                       |  200   |  2055  |