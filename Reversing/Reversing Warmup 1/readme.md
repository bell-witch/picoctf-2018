# Reversing Warmup 1
## Solve
Throughout your journey you will have to run many programs. Can you navigate to /problems/reversing-warmup-1_4_6b2499250c4624337a1948ac374c4934 on the shell server and run this [program](./run) to retreive the flag? 

## Hints
If you are searching online, it might be worth finding how to exeucte a program in command line.

## Solution
all we have to do is run the program to get our flag: `picoCTF{welc0m3_t0_r3VeRs1nG}`