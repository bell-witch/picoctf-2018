# assembly-2
## Solve
What does asm2(0x8,0x21) return? Submit the flag as a hexadecimal value (starting with '0x'). NOTE: Your submission for this question will NOT be in the normal flag format. [Source](./loop_asm_rev.S) located in the directory at /problems/assembly-2_1_c1900e7d33989b0191c51ef927b24f37. 

## Hints
assembly [conditions](https://www.tutorialspoint.com/assembly_programming/assembly_conditions.htm)

## Solution
source
```
asm2:
    push    ebp	
    mov     ebp,esp
    sub     esp,0x10					
    mov     eax,DWORD PTR [ebp+0xc]		
    mov     DWORD PTR [ebp-0x4],eax	
    mov     eax,DWORD PTR [ebp+0x8]		
    mov     DWORD PTR [ebp-0x8],eax			
    jmp     part_b
part_a:	
    add     DWORD PTR [ebp-0x4],0x1		
    add     DWORD PTR [ebp+0x8],0xa9		
part_b:	
    cmp     DWORD PTR [ebp+0x8],0x3923  
    jle     part_a						
    mov     eax,DWORD PTR [ebp-0x4]		
    mov	    esp,ebp	
    pop     ebp
    ret	
```
we can ignore the first few lines which are just setting up the stack. what we're interested in is the rest.  

remember [assembly-0](../assembly-0)? just like before, we're moving the arguments into a register only this time they've been inverted; storing the **second** argument, then the **first** argument
```
    mov     eax,DWORD PTR [ebp+0xc]
    mov     DWORD PTR [ebp-0x4],eax
    mov     eax,DWORD PTR [ebp+0x8]
    mov     DWORD PTR [ebp-0x8],eax
    jmp     part_b
````
now we know where our arguments are stored; on to part_b 
```
part_b:	
    cmp     DWORD PTR [ebp+0x8],0x3923
    jle     part_a
```  
we're not interested in the last few lines yet, but we can see a compare (CMP) instruction, followed by a conditional jump (JLE).  we can see our program is **comparing** what's in ebp+0x8 to 0x3923, and will **jump** to part_a if its **less than or equal to**  
```
part_a:	
    add     DWORD PTR [ebp-0x4],0x1		
    add     DWORD PTR [ebp+0x8],0xa9
```  
we can see in part_a we're **adding** 0x1 to ebp-0x4, and 0xa9 to ebp+0x8.  

if the name of the source wasnt a big enough hint, its pretty obvious we have a simple loop that is checking if ebp+0x8 is less than or equal to 0x3923, and incrementing both of our original values until the comparison is no longer true.  
```
    mov     eax,DWORD PTR [ebp-0x4]		
    mov	    esp,ebp	
    pop	    ebp
    ret	
```  
the last few lines move ebp-0x4 into eax, take care of the stack and then ret. since we know ret will return the value in eax, we can figure out what it is with a short bit of python
```python
var1 = 0x8
var2 = 0x21

while var1 <= 0x3932:
    var2 += 0x1
    var1 += 0xa9

print(hex(var2))
```
giving us our flag: `0x78`