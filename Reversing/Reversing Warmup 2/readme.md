# Reversing Warmup 2
## Solve
Can you decode the following string dGg0dF93NHNfczFtcEwz from base64 format to ASCII?

## Hints
Submit your answer in our competition's flag format. For example, if you answer was 'hello', you would submit 'picoCTF{hello}' as the flag.

## Solution
again using [cyberchef](https://gchq.github.io/CyberChef/) our life is easy, and our flag is: `picoCTF{th4t_w4s_s1mpL3}`