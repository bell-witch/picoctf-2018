# assembly-0
## Solve
What does asm0(0xd8,0x7a) return? Submit the flag as a hexadecimal value (starting with '0x'). NOTE: Your submission for this question will NOT be in the normal flag format. [Source](./intro_asm_rev.S) located in the directory at /problems/assembly-0_1_fc43dbf0079fd5aab87236bf3bf4ac63. 

## Hints
basical assembly [tutorial](https://www.tutorialspoint.com/assembly_programming/assembly_basic_syntax.htm)  
assembly [registers](https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm)

## Solution
source
```
asm0:
    push    ebp
    mov	    ebp,esp
    mov	    eax,DWORD PTR [ebp+0x8]
    mov	    ebx,DWORD PTR [ebp+0xc]
    mov	    eax,ebx
    mov	    esp,ebp
    pop	    ebp	
    ret
```   
the first two lines are setting up the stack, which we arent interested in. the following two show us where our arguments are being stored  
```
    mov	    eax,DWORD PTR [ebp+0x8]     ; moves 0xd8 into eax
    mov	    ebx,DWORD PTR [ebp+0xc]     ; moves 0x7a into ebx
```   
after that, we see a few move(MOV) instructions, but we're only interested in one
```
    mov	    eax,ebx     ; moves 0x7a into eax
```  
the next two lines are stack related, which we arent interested in, leaving us with the ret instruction. since ret will return whatever is in eax, we know our flag: `0x7a`