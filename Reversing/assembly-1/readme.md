# assembly-1
## Solve
What does asm1(0x15e) return? Submit the flag as a hexadecimal value (starting with '0x'). NOTE: Your submission for this question will NOT be in the normal flag format. [Source](./eq_asm_rev.S) located in the directory at /problems/assembly-1_3_cfc4373d0e723e491f368e7bcc26920a. 

## Hints
assembly [conditions](https://www.tutorialspoint.com/assembly_programming/assembly_conditions.htm)
asm1(0x15e)
## Solution
looking at the source we see several compare instructions and conditional jumps. since we dont have anything more complicated than conditional jumps and compares, python will make it pretty easy to get through.  
```
    cmp	DWORD PTR [ebp+0x8],0xdc
    jg 	part_a              ; jump if greater than
```
\>>> 0xdc > 0x15e  
False
```
    cmp	DWORD PTR [ebp+0x8],0x8
    jne	part_b              ; jump if not equal to
```
\>>> 0x8 == 0x15e  
False  

since they're not equal we jump to part_b
```
part_b:
    mov	eax,DWORD PTR [ebp+0x8]
    sub	eax,0x3
    jmp	part_d
```
here we move our argument into eax and subtract 0x3 from it. python makes this easy again.  
\>>> hex(0x15e - 0x3)  
'0x15b'
```
part_d:
    pop	ebp
    ret
```
ignoring the first line, we know that ret will return whats in eax, giving us our flag: `0x15b`