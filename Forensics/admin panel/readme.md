# admin panel
## Solve
We captured some [traffic](./data.pcap) logging into the admin panel, can you find the password? 

## Hints
Tools like wireshark are pretty good for analyzing pcap files.

## Solution
just like the hint suggests we'll use [wireshark](https://www.wireshark.org/) to analyze the pcap file provided  
![wireshark1](img/wireshark1.png)  
looking at the image we see a lot of extra traffic we arent worried about. filtering by http gets us closer  
![wireshark2](img/wireshark2.png)  
as we look through the requests for the /login page we can see the admin login with a passowrd containing our flag: `picoCTF{n0ts3cur3_9feedfbc}`
