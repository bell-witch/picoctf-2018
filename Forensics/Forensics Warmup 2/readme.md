# Forensics Warmup 2
## Solve
Hmm for some reason I can't open this [PNG](./flag.png)? Any ideas? 

## Hints
How do operating systems know what kind of file it is? (It's not just the ending!  
Make sure to submit the flag as picoCTF{XXXXX}

## Solution
opening the image with [irfranview](https://www.irfanview.com/) gives us this error message  
![error](img/error.png)  
hitting yes or renaming the file will open the image and give us our flag: `picoCTF{extensions_are_a_lie}`