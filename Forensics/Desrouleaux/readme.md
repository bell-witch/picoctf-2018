# Desrouleaux
## Solve
Our network administrator is having some trouble handling the tickets for all of of our incidents. Can you help him out by answering all the questions? Connect with nc 2018shell3.picoctf.com 10493. [incidents.json](./incidents.json)

## Hints
If you need to code, python has some good libraries for it.

## Solution
this one is pretty straight forward. itll ask a bunch of questions, and all the information you need is in the json file. it changes the questions slightly each time you connect so just pay attention. not sure why this is included in the CTF (unless they wanted you to bruteforce it?)
```
You'll need to consult the file `incidents.json` to answer the following questions.

What is the most common source IP address? If there is more than one IP address that is the most common, you m
ay give any of the most common ones.
248.0.40.3
Correct!

How many unique destination IP addresses were targeted by the source IP address 248.0.40.3?
4
Correct!

What is the average number of unique destination IP addresses that were sent a file with the same hash? Your a
nswer needs to be correct to 2 decimal places.
1.28
Correct!

Great job. You've earned the flag: picoCTF{J4y_s0n_d3rUUUULo_4f3aae0d}
```
probably my least favorite problem of the ones i completed, but our flag is: `picoCTF{J4y_s0n_d3rUUUULo_4f3aae0d}`