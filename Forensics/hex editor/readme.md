# hex editor
## Solve
This [cat](./hex_editor.jpg) has a secret to teach you. You can also find the file in /problems/hex-editor_0_8c20f979e6b2740dee597871ff1a74ee on the shell server. 

## Hints
What is a hex editor?  
Maybe google knows.  
[xxd](http://linuxcommand.org/man_pages/xxd1.html)  
[hexedit](http://linuxcommand.org/man_pages/hexedit1.html)  
[bvi](http://manpages.ubuntu.com/manpages/natty/man1/bvi.1.html)  

## Solution
after opening the file in hexedit we hit tab to toggle from hex to ascii, CTRL+S to search for the word "pico", and then we have our flag: `picoCTF{and_thats_how_u_edit_hex_kittos_3E03e57d}`