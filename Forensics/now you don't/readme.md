# now you don't
## Solve
We heard that there is something hidden in this [picture](./nowYouDont.png). Can you find it?

## Hints
There is an old saying: if you want to hide the treasure, put it in plain sight. Then no one will see it.  
Is it really all one shade of red?

## Solution
like the hint suggests, there's more going on in the image than meets the eye. uploading it [here](https://29a.ch/photo-forensics/#forensic-magnifier) and using error level analysis will show us the flag: `picoCTF{n0w_y0u_533_m3}`