# Truly an Artist
## Solve
Can you help us find the flag in this [Meta-Material](./2018.png)? You can also find the file in /problems/truly-an-artist_4_cdd9e325cf9bacd265b98a7fe336e840. 

## Hints
Try looking beyond the image.  
Who created this?

## Solve
there are several ways to solve this but i used strings and grep; `strings ./2018.png | grep "pico"` to get the flag: `picoCTF{look_in_image_13509d38}`