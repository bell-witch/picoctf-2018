# What's My Name?
## Solve
Say my name, say [my name](./myname.pcap). 

## Hints
If you visited a website at an IP address, how does it know the name of the domain?

## Solution
with just over 6100 packets captured its far too much to go through by hand. looking at the hints it mentions [DNS](https://www.cloudflare.com/learning/dns/what-is-dns/). filtering by `dns` brings it down to two. looking through the response we can easily find our flag: `picoCTF{w4lt3r_wh1t3_2d6d3c6c75aa3be7f42debed8ad16e3b}`
