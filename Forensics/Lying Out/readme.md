# Lying Out
## Solve
Some odd [traffic](./traffic.png) has been detected on the network, can you identify it? More [info](./info.txt) here. Connect with nc 2018shell3.picoctf.com 27108 to help us answer some questions. 

## Hints
None!

## Solution
here's what mine looked like
```
You'll need to consult the file `traffic.png` to answer the followin
g questions.

Which of these logs have significantly higher traffic than is usual 
for their time of day? You can see usual traffic on the attached plo
t. There may be multiple logs with higher than usual traffic, so ans
wer all of them! Give your answer as a list of `log_ID` values separ
ated by spaces. For example, if you want to answer that logs 2 and 7
 are the ones with higher than usual traffic, type 2 7.
    log_ID      time  num_IPs
0        0  01:15:00     9774
1        1  02:30:00    11582
2        2  05:30:00    11619
3        3  06:45:00    10503
4        4  07:15:00    10032
5        5  07:30:00    10848
6        6  07:45:00    14773
7        7  07:45:00    10937
8        8  08:30:00    10933
9        9  09:00:00     9573
10      10  09:15:00    11566
11      11  16:00:00    10067
12      12  18:00:00    11511
13      13  23:15:00     9801
1 2 6 10 
Correct!

Great job. You've earned the flag: picoCTF{w4y_0ut_de051415}
```       
this is another question that i dont think really belongs here. you could certainly bruteforce it like the [last](../Desrouleaux) one, but looking through the chart gives you all the information you need to get the flag: `picoCTF{w4y_0ut_de051415}`