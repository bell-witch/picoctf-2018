# Reading Between the Eyes
## Solve
Stego-Saurus hid a message for you in this [image](./husky.png), can you retreive it? 

## Hints
Maybe you can find an online decoder?

## Solution
using [this](http://stylesuxx.github.io/steganography/) tool (i dont really care for this kind of stuff that much) lets us get the flag: `picoCTF{r34d1ng_b37w33n_7h3_by73s}`