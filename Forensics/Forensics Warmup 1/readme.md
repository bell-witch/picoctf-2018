# Forensics Warmup 1
## Solve
Can you unzip this [file](./flag.zip) for me and retreive the flag? 

## Hints
Make sure to submit the flag as picoCTF{XXXXX}

## Solution
unzipping the file reveals an image. simply open the image and get our flag: `picoCTF{welcome_to_forensics}`