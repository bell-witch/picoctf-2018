# grep 2
## Solve
This one is a little bit harder. Can you find the flag in /problems/grep-2_2_413a577106278d0711d28a98f4f6ac28/files on the shell server? Remember, grep is your friend. 

## Hints
grep [tutorial](https://ryanstutorials.net/linuxtutorial/grep.php)

## Solution
instead of going through every directory and file by hand, using `grep -r "pico"` will do it for us and find the flag: `picoCTF{grep_r_and_you_will_find_8eb84049}`