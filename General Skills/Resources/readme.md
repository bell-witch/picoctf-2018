# Resources
## Solve
We put together a bunch of resources to help you out on our website! If you go over there, you might even find a flag! https://picoctf.com/resources ([link](https://picoctf.com/resources)) 

## Hints
None!

## Solve
scrolling to the bottom of the page gives us our flag: `picoCTF{xiexie_ni_lai_zheli`