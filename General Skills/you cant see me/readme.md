# you can't see me
## Solve
'...reading transmission... Y.O.U. .C.A.N.'.T. .S.E.E. .M.E. ...transmission ended...' Maybe something lies in /problems/you-can-t-see-me_2_cfb71908d8368e3062423b45959784aa. 

## Hints
What command can see/read files?  
What's in the manual page of ls?  

## Solution
after we cd in and use ls, nothing shows up. using ls with the flags `-al` shows us there's a file with a name we arent able to type. the easy solution for this is to type `cat` and hit tab to auto-fill the name and give us our flag: `picoCTF{j0hn_c3na_paparapaaaaaaa_paparapaaaaaa_093d6aff}`