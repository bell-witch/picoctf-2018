# net cat
## Solve
Using netcat (nc) will be a necessity throughout your adventure. Can you connect to 2018shell3.picoctf.com at port 10854 to get the flag? 

## Hints
nc [tutorial](https://linux.die.net/man/1/nc)

## Solution
using the command `nc 2018shell3.picoctf.com 10854` will get us our flag: `picoCTF{NEtcat_iS_a_NEcESSiTy_c97963fe}`