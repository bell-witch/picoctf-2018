# pipes
## Solve
During your adventure, you will likely encounter a situation where you need to process data that you receive over the network rather than through a file. Can you find a way to save the output from this program and search for the flag? Connect with 2018shell3.picoctf.com 37542. 

## Hints
Remember the flag format is picoCTF{XXXX}  
Ever heard of a pipe? No not that kind of pipe... This [kind](http://www.linfo.org/pipes.html)

## Solution
using pipes and grep like so: `nc 2018shell2.picoctf.com 44310 | grep pico` lets us get our flag: `picoCTF{almost_like_mario_a6975cdb}`