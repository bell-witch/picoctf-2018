# General Warmup 3
## Solve
What is 0x3D (base 16) in decimal (base 10). 

## Hints
Submit your answer in our competition's flag format. For example, if you answer was '22', you would submit 'picoCTF{22}' as the flag.

## Solution
again, python makes this simple
```python
0x3d
61
```
giving us our flag: `picoCTF{61}`