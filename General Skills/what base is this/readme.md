# what base is this?
## Solve
To be successful on your mission, you must be able read data represented in different ways, such as hexadecimal or binary. Can you get the flag from this program to prove you are ready? Connect with nc 2018shell3.picoctf.com 14390. 

## Hints
I hear python is a good means (among many) to convert things.  
It might help to have multiple windows open

## Solution
when we connect it gives us several questions and asks us to convert from different things. like the hint suggests, python will work fine, but since time isnt *too* much of an issue, i used [cyberchef](https://gchq.github.io/CyberChef/). heres what mine looked like
```
We are going to start at the very beginning and make sure you understand how data is stored.
Please give me the 01110111 01101001 01100011 01101000 01101001 01110100 01100001 as a word.
To make things interesting, you have 30 seconds.
Input:
wichita
Please give me the 726f626f74 as a word.
Input:
robot
Please give me the  143 157 165 143 150 as a word.
Input:
couch
You got it! You're super quick!
Flag: picoCTF{delusions_about_finding_values_602fd280}
```

with our flag, `picoCTF{delusions_about_finding_values_602fd280}`