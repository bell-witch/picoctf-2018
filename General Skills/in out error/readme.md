# in out error
## Solve
Can you utlize stdin, stdout, and stderr to get the flag from this [program](./in-out-error)? You can also find it in /problems/in-out-error_1_24ebc7186086f0f9a710de008628c561 on the shell server
  
## Hints
Maybe you can split the stdout and stderr output?

## Solution
running the program gives us a lot of gibberish
```Hey There!
If you want the flag you have to ask nicely for it.
Enter the phrase "Please may I have the flag?" into stdin and you shall receive.
Please may I have the flag?
Thank you for asking so nicely!


pWiec'orCeT Fn{op 1spt1rnagn_g1eSr_s4 _t7oh 1lnogv_e7
bY9o3u6 0kcnao}wp itchoeC TrFu{lpe1sp 1anngd_ 1sSo_ 4d_o7 hI1
nAg _f7ubl9l3 6c0ocmam}iptimceonCtT'Fs{ pw1hpa1tn gI_'1mS _t4h_i7nhk1inngg_ 7obf9
3Y6o0uc aw}opuilcdonC'TtF {gpe1tp 1tnhgi_s1 Sf_r4o_m7 ha1nnyg _o7tbh9e3r6 0gcuay}
p
iIc ojCuTsFt{ pw1apn1nnag _t1eSl_l4 _y7ohu1 nhgo_w7 bI9'3m6 0fceae}lpiincgo
CGToFt{tpa1 pm1ankge_ 1ySo_u4 _u7nhd1enrgs_t7abn9d3
```
following the hint we can split stdout and stderr very easily by redirecting output to `/dev/null`
```
dog@pico-2018-shell-3:/problems/in-out-error_1_24ebc7186086f0f9a710de008628c561$ ./in-out-error > /dev/null
Please may I have the flag?
picoCTF{p1p1ng_1S_4_7h1ng_7b9360ca}picoCTF{p1p1ng_1S_4_7h1ng_7b9360ca}picoCTF{p1p1ng_1S_4_7h1ng_7b9360ca}picoC
TF{p1p1ng_1S_4_7h1ng_7b9360ca}picoCTF{p1p1ng_1S_4_7h1ng_7b9360ca}picoCTF{p1p1ng_1S_4_7h1ng_7b9360ca}picoCTF{p1
p1ng_1S_4_7h1ng_7b9360ca}picoCTF{p1p1ng_1S_4_7h1ng_7b9360ca}picoCTF{p1p1ng_1S_4_7h1ng_7b9360ca}
```
this leaves us with our flag: `picoCTF{p1p1ng_1S_4_7h1ng_7b9360ca}`