# absolutely relative
## Solve
In a filesystem, everything is relative ¯\\\_(ツ)_/¯. Can you find a way to get a flag from this [program](./absolutely-relative)? You can find it in /problems/absolutely-relative_3_c1a43555f1585c98aab8d5d2c7f0f9cc on the shell server. [Source](./absolutely-relative.c). 

## Hints
Do you have to run the program in the same directory? (⊙.☉)7  
Ever used a text editor? Check out the program 'nano'

## Solution
source:
```c
#include <stdio.h>
#include <string.h>

#define yes_len 3
const char *yes = "yes";

int main()
{
    char flag[99];
    char permission[10];
    int i;
    FILE * file;


    file = fopen("/problems/absolutely-relative_3_c1a43555f1585c98aab8d5d2c7f0f9cc/flag.txt" , "r");
    if (file) {
    	while (fscanf(file, "%s", flag)!=EOF)
    	fclose(file);
    }   
	
    file = fopen( "./permission.txt" , "r");
    if (file) {
    	for (i = 0; i < 5; i++){
            fscanf(file, "%s", permission);
        }
        permission[5] = '\0';
        fclose(file);
    }
    
    if (!strncmp(permission, yes, yes_len)) {
        printf("You have the write permissions.\n%s\n", flag);
    } else {
        printf("You do not have sufficient permissions to view the flag.\n");
    }
    
    return 0;
}
```
looking at the source we can see the program is opening the file `permission.txt` in the directory the program is run from. we can also see that the contents of permission.txt are being compared to "yes" with a total size of '3'. all we have to do is move into a directory where we can write to a file, use something like `echo "yes" > permission.txt`,  run the program and get our flag: `picoCTF{3v3r1ng_1$_r3l3t1v3_6193e4db}`