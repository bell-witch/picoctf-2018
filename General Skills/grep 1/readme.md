# grep 1
## Solve
Can you find the flag in [file](./file)? This would be really obnoxious to look through by hand, see if you can find a faster way. You can also find the file in /problems/grep-1_2_ee2b29d2f2b29c65db957609a3543418 on the shell server.

## Hints
grep [tutorial](https://ryanstutorials.net/linuxtutorial/grep.php)

## Solution
running `grep "picoCTF" file` will net us our flag: `picoCTF{grep_and_you_will_find_42783683}`