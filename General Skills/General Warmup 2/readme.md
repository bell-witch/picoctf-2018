# General Warmup 2
## Solve
Can you convert the number 27 (base 10) to binary (base 2)? 

## Hints
Submit your answer in our competition's flag format. For example, if you answer was '11111', you would submit 'picoCTF{11111}' as the flag.

## Solution
using python makes this very easy
```python 
bin(27)
'0b11011'
```
which gives us our flag: `picoCTF{11011}`