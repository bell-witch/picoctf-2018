# strings
## Solve
Can you find the flag in this [file](./file) without actually running it? You can also find the file in /problems/strings_0_bf57524acf558aca2081eb97ece8e2ee on the shell server. 

## Hints
[strings](https://linux.die.net/man/1/strings)

## Solution
just using the command `strings` gives us a ton of useless stuff
```
vZw4bmo4msd8fSrCENtEpkv1lQZx6Dz5yc                                
2EUUYEggTuJqb8cLSdzrRD5Ng5MNtHyDfvrFb6z                           
mLg2jEVzPHlgZFz8lXaFWqxV6Gmb                                      
Kl6xULDHSwE8rnRPOfcpNPWsi8xMGaASzJCl3Offe4                        
4uyyytwQJ23S2YpBeD3tQmJan1KxhnintLpZ                              
6d43CL8FxGrwbox4f7d                                               
uhSR8ky7sFCmQACHToxKq95z                                          
KkHtMWXDEyAbsMrSgeurLBgMSWSzXUK                                   
nOMLoW1YOCnd15mBOs1KVRE0weZYeXqNCqdGy                             lAgCJaa1fpvxUysLRWqkHn                                            
...
```
so to filter out all the noise we use `strings strings | grep "picoCTF"` and we get the flag: `picoCTF{sTrIngS_sAVeS_Time_c09b1444}`