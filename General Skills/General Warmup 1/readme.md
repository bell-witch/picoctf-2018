# General Warmup 1
## Solve
If I told you your grade was 0x41 in hexadecimal, what would it be in ASCII? 

## Hints
Submit your answer in our competition's flag format. For example, if you answer was 'hello', you would submit 'picoCTF{hello}' as the flag.

## Solution
there are a million ways to turn hex into ascii, but i prefer to use [cyberchef](https://gchq.github.io/CyberChef/). using any of the millions of ways will get us our flag: `picoCTF{A}`