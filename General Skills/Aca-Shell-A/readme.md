# Aca-Shell-A
## Solve
It's never a bad idea to brush up on those linux skills or even learn some new ones before you set off on this adventure! Connect with nc 2018shell3.picoctf.com 33158. 

## Hints
Linux for [Beginners](https://maker.pro/education/basic-linux-commands-for-beginners)

## Solution

``` 
nc 2018shell3.picoctf.com 33158
Sweet! We have gotten access into the system but we aren't root.
It's some sort of restricted shell! I can't see what you are typing
but I can see your output. I'll be here to help you along.
If you need help, type "echo 'Help Me!'" and I'll see what I can do
There is not much time left!  

~/$ ls
blackmail
executables
passwords
photos
secret  

~/$ cd secret
Now we are cookin'! Take a look around there and tell me what you find!  

~/secret$ ls
intel_1
intel_2
intel_3
intel_4
intel_5
profile_ahqueith5aekongieP4ahzugi
profile_ahShaighaxahMooshuP1johgo
profile_aik4hah9ilie9foru0Phoaph0
profile_AipieG5Ua9aewei5ieSoh7aph
profile_bah9Ech9oa4xaicohphahfaiG
profile_ie7sheiP7su2At2ahw6iRikoe
profile_of0Nee4laith8odaeLachoonu
profile_poh9eij4Choophaweiwev6eev
profile_poo3ipohGohThi9Cohverai7e
profile_Xei2uu5suwangohceedaifohs
Sabatoge them! Get rid of all their intel files!  

~/secret$ rm int*
Nice! Once they are all gone, I think I can drop you a file of an exploit!
Just type "echo 'Drop it in!' " and we can give it a whirl!  

~/secret$ echo 'Drop it in!'
Drop it in!                                                                                                   
I placed a file in the executables folder as it looks like the only place we can execute from!
Run the script I wrote to have a little more impact on the system!  

~/secret$ cd ..  

~/$ ls
blackmail                                                                                                     
executables                                                                                                   
passwords                                                                                                     
photos                                                                                                        
secret  
                                                                  
~/$ cd executables  

~/executables$ ls
dontLookHere  

~/executables$ ./dontLookHere
728a 00ca c053 f486 6d9a ddf3 6b05 2c5d c2ae 0b5a f6ad 28f3 424f 51ef 2467 7102 c828 7768 b0a2 57ea fedb b196 bae5 1525 07f6 7bdf 2e01 edac 7afc 47bd 3003 b806 4cd2 8b73 be27 0ecf 2011 a3e0 ccd8 23bb 591b 7fe9 03ec a81e fe44 2dd5 c858 4c98 dc9e 135e b2b7 4c3f fc1f 2c86 3a20 e496 85d7 a5b4 c668 ace3 b022 e96a 65ba 448f cc74 226d 67a5 39c0 1181 4d6a 6684 62a9 f4b0 8a53 73da bc05 0368 f3c0 cf49 25ef 1842 fe19 3e5a 2a12 98ee d100 b041 5760 74ad abfe a4e0 2345 e64d 6760 fd94 bfbe 5908 48b4 81d3 0185 a4d1 999a f584 b753 9167 227a 5531 9c81 16d0 8033 8323 53d0 51a1 5d60 1dcf e7dc 4b50 e9d6 4403 a41e ea6f 3422 fbd5 9585 a9b6 53cf 18d4 e661 d06a 0508 a25c 082a c44c ac66 4810 aa38 ac87 7c64 e4a4 1e44 67ef 92ec 3103 e366 eac5 9dad d91f 6dcc c7a0 575b e59c d017 dc44 430d 89be 87a4 9614 3a97 e72b 7ae6 63a4 324a 0e30 c4a3 1d0d 8be2 f24d 51a1 4a20 ce5b 6a5c 4c37 09a1 0953 d21d 2334 8b17 2890 ff10 d9d2 38ee b0db 47e2 edcb 5cd3 e422 f71d 8abe 82ff c1b4 de0f 6bd0 1dc4 82f5 8d60 1939 3995 3084 ad47 d7c2 2945 0168 df01 6e2a 65ff 92df a2b4 8c6c cb5c d4a6 c25c 0004 3e1b 0d40 669b 8f7c d1b6 fa2c b049 8502 51b1 fcbb 86db aaf1 0e17 4db9 772a b9f9 a7d3 cab5 1d5f 6354 b087 ee49 8f18 01c0 835f 8712 0d58 ca99 6490 4a04 f4a9 f243 5244 12cf c238 3076 f3a5 d5ca cfce 6b51 b143 f0e9 a4ff 56b7 a352 2bed af01 bf12 7715 7464 d30b f39f dcb3 1bec 3569 71bc d8da a07e d34a 9caa 77fd 848f b92a e2a1 91c3 44ba 04ab b6ae 86a5 b929 0136 1b84 9532 b77c 228d 43e0 adbf 8375 c7a4 a8ef ac11 f0c0 c46c 0ba6 7b9e fdf4 8006 1470 96de 721b cb64 c9a3 3532 83d4 4e43 ca9c 5af1 4125 2346 3f89 97ea b418 f0bd a567 d2d3 b7da b426 ccd4 01e6 96ef 5007 5412 af2b a92c 4601 ea1e 463b 195b 226b 079c d817 04c6 ca2f 87cf 1203 9310 e1c5 b5b5 fca4 e030 27a8 d8bf ce63 4731 a665 46a8 8b04 fa3b 5bcf 0ac6 78d1 c98c 1746 bf5a ab2e e2a3 ce84 5c81 963c 1a86 1abc f24c e0e5 0922 31c2 08c7 4a27 d6b6 b795 e570 bf6e ea22 ed00 8a21 3eaa ea7c 1079 e2b7 1790 5ffe b217 9ba1 0202 2c58 4695 486c 754a bf04 dee8 024f db4a 1b3e 498a 7505 0337 23b7 67bb 6527 6079 4778 18d9 1d0b 5dd1 b2ba 1047 4150 55ab c9d6 4f21 e9e7 4352 d46c fa18 9e00 d86c d00b 767f 5e39 f862 8c53 2151 2bf3 9bb9 f54e 652b 902f 695b 4ed0 8018 c9f3 c613 16c3 7da7 7dde 8ed9 228a 9633 84fd b33f e8d2 1de0 ed5d 6699 416c 0655 9add 48d6 171b 14a7 fbd6 f005 dafb 0eae f40f 7b82 b523 0c99 d676 b1e1 2eae bda6 fdec 9dc2 da1c 1262 947d 0754 605f 4932 e143 6a71 5a59 5474 c870 3726 36ba 48da bb37 32b3 02aa 002f 16cc c9c2 92f4 24b6 da68 1f6a f373 1b10 2798 ebad 4446 5575 f981 d6a3 1a69 1296 6330 26bf ea4a aa0a 2864 3002 3203 477a b06b a8ac 089c d524 dbb8 0963 f4fd 9503 bd6a 452b b9e6 7bed a240 b72e 94cb d659 435d 6f91 0c5a f03b 2197 f0f5 7400 945d b2c8 a27a 3b81 ce58 2e4e 3e26 f43d 8607 3141 5e00 ae4e 1660 7c48 675e 5f38 2819 96a1 cbcf 138c 7d7d eae3 c4a0 569c e314 00fb ce66 5307 7cdb c99b 562b d5ff 77b7 8334 086b 932b b1b0 a260 ae91 aff2 fb47 fb25 dd12 d466 361d 27de f10f 4ff4 a6a9 f565 e6ce 7160 6f1a 7668 c4bb 721c 4acc e928 923f bdbe 756f e024 28f2 194b 9dbb 3bd9 7cf7 28af c056 8d1e fbd2 fb96 f309 5551 726c 325f e300 17b9 c787 e7b0 76dd b2f9 1501 7e04 5957 6f81 7021 8955 bb66 d356 5979 940a ed49 4281 4827 8e1e 083e 62e0 1cc0 ebef a315 c007 836e bee0 0ac2 3140 1dcc 9a9f 6392 b757 7b58 1cd2 070f 3c24 fd86 d5fd 52a1 7d50 e1d7 14c7 efcc 5955 174c af4d b80d 00b3 f0c7 e82d 3734 0d47 533a ea3f 8b5a 1797 2f3d 7a9c 9006 a10a c646 27e8 e2e1 17b9 300e 1fdd 7609 d9f9 9d31 dc71 3039 3063 8bb4 5084 2916 4ffd 827f f566 0fb8 fef7 a326 4330 8598 bae9 9aea d1cc ca72 8c1a 7d66 aff3 84a4 7ab6 3f86 28bc 949f e3f3 33dd 78ed 8fe7 347e e03a 6ea0 5285 1e35 acfd 8593 5e06 30b6 e307 29ff 7961 a80e 52ac 9c4a c233 2088 84c8 b0e6 fde4 23f2 0493 dd83 df04 3434 990c cd4b 1945 32f4 dc17 0c17 bdf6 499c 8516 781d c62e 1308 5445 5402 fd95 8d26 eb0d a6fd 7dc9 538d a10e 86ca a3d6 7eb7 b3eb 1d70 4845 d683 33d2 c0ac 89f8 a56b 1062 38ec 5cf7 8b5d 4b24 1fbb e745 3c72 44a9 423a 3661   
Looking through the text above, I think I have found the password. I am just having trouble with a username.
Oh drats! They are onto us! We could get kicked out soon!
Quick! Print the username to the screen so we can close are backdoor and log into the account directly!
You have to find another way other than echo!  

~/executables$ whoami
l33th4x0r                                                                                                     
Perfect! One second!
Okay, I think I have got what we are looking for. I just need to to copy the file to a place we can read.
Try copying the file called TopSecret in tmp directory into the passwords folder.  

~/executables$ cp /tmp/TopSecret ../passwords
Server shutdown in 10 seconds...                                                                              
Quick! go read the file before we lose our connection!  

~/executables$ cd ..  

~/$ cd passwords  

~/passwords$ ls
TopSecret  

~/passwords$ cat TopSecret
Major General John M. Schofield's graduation address to the graduating class of 1879 at West Point is as follows: The discipline which makes the soldiers of a free country reliable in battle is not to be gained by harsh or tyrannical treatment.On the contrary, such treatment is far more likely to destroy than to make an army.It is possible to impart instruction and give commands in such a manner and such a tone of voice as to inspire in the soldier no feeling butan intense desire to obey, while the opposite manner and tone of voice cannot fail to excite strong resentment and a desire to disobey.The one mode or other of dealing with subordinates springs from a corresponding spirit in the breast of the commander.He who feels the respect which is due to others, cannot fail to inspire in them respect for himself, while he who feels,and hence manifests disrespect towards others, especially his subordinates, cannot fail to inspire hatred against himself.
picoCTF{CrUsHeD_It_9edaa84a}                               
```
which gives us our flag: `picoCTF{CrUsHeD_It_9edaa84a}`